<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Custom vars
Config::set('api_href', 'http://localhost:8000');

Route::get('/', function() {
    return view('song-lists.list-all');
});

Route::get('song-lists/create', function() {
    return view('song-lists.create');
});

Route::get('song-lists/{id}', function() {
    return view('song-lists.show');
});

Route::get('song-lists/edit/{id}', function() {
    return view('song-lists.edit');
});
