@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col">
			<h2>Listing all song lists</h2>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<ul id="song-lists-list" class="list-group">
				<p class="loading">Loading...</p>
			</ul>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$(document).ready( function() {
			getSongLists();

			function getSongLists() {
				$.get(
					'{{ Config::get("api_href") }}/lists',
					{},
					function(res) {
						let lists = ''
						$(res).each( function(index, item) {
							lists += `
								<li class="list-group-item">
									<div class="list-info">
										<a href="/song-lists/${item.id}">${item.name}</a><br>
										${formatDate(item.date)}
									</div>
									<div>
										<div class="action-container">
											<a href="/song-lists/edit/${item.id}">
												<i class="fa fa-edit edit-list-btn"></i>
											</a>
											<i class="delete-list-btn fa fa-trash" data-list-id="${item.id}"></i>
										</div>
									</div>
								</li>
							`
						});
						$('#song-lists-list').html(lists);
					}
				);
			}

			$('#song-lists-list').on('click', '.delete-list-btn', function() {
				if (confirm('Are you sure you want to delete this list?')) {
					let list_id = $(this).data('list-id');

					$.ajax({
						method: 'DELETE',
						url: '{{ Config::get("api_href") }}/lists/'+ list_id
					}).done( function() {
						getSongLists();
					});
				}
			});
		});
	</script>
@endsection