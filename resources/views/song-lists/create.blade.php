@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col">
			<h2>New song list</h2>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<form action="" id="newSongListForm">
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" name="name" required />
					<label for="date">Date:</label>
					<input type="date" class="form-control" name="date" required />
				</div>
				<input type="submit" value="Submit" class="btn btn-primary" />
			</form>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$(document).ready( function() {
			$('#newSongListForm').submit( function(e) {
				e.preventDefault();

				$.post(
					'{{ Config::get("api_href") }}/lists',
					{
						name: $('[name="name"]').val(),
						date: $('[name="date"]').val()
					},
					function(res) {
						if (res.id) {
							alert('Song list created successfully!');
							window.location = '/song-lists/edit/'+ res.id;
						}
					}
				);
			});
		});
	</script>
@endsection