@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col">
			<h2>Loading...</h2>
			<button class="btn btn-sm btn-secondary" id="go-metronome-btn">Go to Metronome</button>
			<br><br>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<form action="" id="editSongListForm">
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" name="name" required />
				</div>
				<div class="form-group">
					<label for="date">Date:</label>
					<input type="date" class="form-control" name="date" required />
				</div>
				<input type="submit" value="Submit" class="btn btn-primary" />
			</form>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col">
			<form action="" id="addSongForm">
				<h4>Add new song</h4>
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" name="name" />
				</div>
				<div class="form-group">
					<label for="bpm">BPM:</label>
					<input type="text" class="form-control" name="bpm" />
				</div>
				<div class="form-group">
					<label for="time-signature">Time signature:</label>
					<select name="time-signature" class="form-control">
						<option value="4/4" selected>4/4</option>
						<option value="6/8">6/8</option>
						<option value="3/4">3/4</option>
					</select>
				</div>
				<div class="form-submit">
					<input type="submit" value="Add" class="btn btn-secondary" />
				</div>
			</form>

			<form action="" id="editSongForm" style="display:none">
				<h4>Edit song</h4>
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" class="form-control" name="name" />
				</div>
				<div class="form-group">
					<label for="bpm">BPM:</label>
					<input type="text" class="form-control" name="bpm" />
				</div>
				<div class="form-group">
					<label for="time-signature">Time signature:</label>
					<select name="time-signature" class="form-control">
						<option value="4/4">4/4</option>
						<option value="6/8">6/8</option>
						<option value="3/4">3/4</option>
					</select>
				</div>
				<div class="form-submit">
					<input type="submit" value="Submit" class="btn btn-secondary" />
					<button class="btn btn-tertiary" id="cancel-edit-song-btn">Cancel</button>
				</div>
			</form>

			<hr>
			<h4>Songs</h4>
			<ul id="songs" class="list-group edit-songs">
				<p class="loading">Loading...</p>
			</ul>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$(document).ready( function() {
			let song_list = null;
			getSongList(window.location.href.substr(window.location.href.lastIndexOf('/') + 1));

			function getSongList(id) {
				$.get(
					'{{ Config::get("api_href") }}/lists/'+ id,
					{},
					function(songList) {
						if (songList.id) {
							song_list = songList;
							updateFields();
						}
					}
				);
			};

			function updateFields() {
				$('h2').text('Editing \''+ song_list.name +'\'');
				$('#editSongListForm [name="name"]').val(song_list.name);
				$('#editSongListForm [name="date"]').val(song_list.date);

				updateSongs();
			}

			$('#go-metronome-btn').click( function() {
				window.location = '/song-lists/'+ song_list.id;
			});

			$('#editSongListForm').submit( function(e) {
				e.preventDefault();

				$.ajax({
					method: 'PUT',
					url: '{{ Config::get("api_href") }}/lists/'+ song_list.id,
					data: {
						name: $('#editSongListForm [name="name"]').val(),
						date: $('#editSongListForm [name="date"]').val()
					}
				}).done( function(res) {
					if (res.id) {
						window.location = window.location;
					}
				});
			});

			$(document).on('submit', '#addSongForm', function(e) {
				e.preventDefault();

				$.ajax({
					method: 'POST',
					url: '{{ Config::get("api_href") }}/songs',
					data: {
						name           : $('#addSongForm [name="name"]').val(),
						bpm            : $('#addSongForm [name="bpm"]').val(),
						time_signature : $('#addSongForm [name="time-signature"]').val(),
						song_list_id   : song_list.id
					}
				}).done( function(res) {
					if (res.id) {
						$('#addSongForm [name="name"]').val('').focus();
						$('#addSongForm [name="bpm"]').val('');
						$('#addSongForm [name="time-signature"]').val('4/4');

						getSongList(song_list.id);
					}
				});
			});

			let songEdit = {};
			$(document).on('submit', '#editSongForm', function(e) {
				e.preventDefault();

				$.ajax({
					method: 'PUT',
					url: '{{ Config::get("api_href") }}/songs/'+ songEdit.id,
					data: {
						name           : $('#editSongForm [name="name"]').val(),
						bpm            : $('#editSongForm [name="bpm"]').val(),
						time_signature : $('#editSongForm [name="time-signature"]').val(),
						song_list_id   : song_list.id
					}
				}).done( function(res) {
					if (res.id) {
						$('#editSongForm').trigger('reset')
						getSongList(song_list.id);
						toggleFormDisplay();
					}
				});
			});

			function toggleFormDisplay() {
				$('#addSongForm').toggle();
				$('#editSongForm').toggle();
			}

			$('#cancel-edit-song-btn').click( function() {
				$('#editSongForm').trigger('reset');
				toggleFormDisplay();
			});

			function updateSongs() {
				let songs_html = '';
				$(song_list.songs).each( function(index, item) {
					songs_html += `
						<li class="list-group-item song-item" data-song-id="${item.id}" data-song-name="${item.name}" data-song-bpm="${item.bpm}" data-song-time_signature="${item.time_signature}">
							<div class="song-info">
								<strong>${index + 1})</strong> ${item.name} <strong>(${item.bpm})</strong> - ${item.time_signature}
							</div>
							<div>
								<div class="action-container">
									<i class="edit-song-btn fa fa-edit"></i>
									<i class="delete-song-btn fa fa-trash"></i>
								</div>
							</div>
						</li>
					`;
				});
				$('#songs').html(songs_html);
			}

			$('#songs').on('click', '.edit-song-btn', function() {
				let $song_item = $(this).parents('.song-item');
				songEdit = {
					id: $song_item.data('song-id'),
					name: $song_item.data('song-name'),
					bpm: $song_item.data('song-bpm'),
					time_signature: $song_item.data('song-time_signature')
				};

				$('#editSongForm').find('[name="name"]').val(songEdit.name);
				$('#editSongForm').find('[name="bpm"]').val(songEdit.bpm);
				$('#editSongForm').find('[name="time-signature"]').val(songEdit.time_signature);

				toggleFormDisplay();

				$([document.documentElement, document.body]).animate({
					scrollTop: $("#editSongForm").offset().top - 16
				}, 150);
				$('#editSongForm').find('[name="name"]').focus();
			});

			$('#songs').on('click', '.delete-song-btn', function() {
				if (confirm('Are you sure you want to delete this song?')) {
					let song_id = $(this).parents('.song-item').data('song-id');

					$.ajax({
						method: 'DELETE',
						url: '{{ Config::get("api_href") }}/songs/'+ song_id
					}).done( function() {
						getSongList(song_list.id);
					});
				}
			});
		});
	</script>
@endsection