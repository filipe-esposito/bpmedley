@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col">
			<h2>Loading...</h2>
			<div id="date"></div>
			<button id="edit-song-list-btn" class="btn btn-sm btn-secondary">Edit</button>
		</div>
	</div>
	<div class="row metronome-btns align-items-center d-flex justify-content-center">
		<i id="prev-btn" class="fa fa-chevron-circle-left inactive"></i>
		<i id="play-btn" class="fa fa-play-circle"></i>
		<i id="next-btn" class="fa fa-chevron-circle-right inactive"></i>
	</div>
	<div class="row">
		<div class="col metronome-display">
			<span class="metronome-change-btn minus">-</span>
			<span id="bpm-value">60</span>
			<span class="metronome-change-btn plus">+</span>
		</div>
	</div>
	<div class="row">
		<div class="col metronome-steps align-items-center d-flex justify-content-center">
			<span class="i"></span>
			<span class="ii"></span>
			<span class="iii"></span>
			<span class="iv"></span>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<ul id="songs" class="list-group">
				<p class="loading">Loading songs...</p>
			</ul>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$(document).ready( function() {
			let songList_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
			getSongList(songList_id);

			function getSongList(id) {
				$.get(
					'{{ Config::get("api_href") }}/lists/'+ id,
					{},
					function(songList) {
						if (songList.id) {
							$('h2').text(songList.name);
							$('#date').text(formatDate(songList.date));

							let $song_list = '';
							$(songList.songs).each( function(index, item) {
								$song_list += `
									<li class="list-group-item song-item" data-song-id="${item.id}" data-bpm="${item.bpm}" data-time_signature="${item.time_signature}">
										<strong>${index + 1})</strong> ${item.name} <strong>(${item.bpm})</strong> - ${item.time_signature}
									</li>
								`;
							});
							$('#songs').html($song_list);
							song_list_length = $(songList.songs).length;
							updatePrevNextBtns();
						}
					}
				);
			};

			let song_list_length = 0;
			let current_song_pos = null;

			let metronome_interval = null;
			let current_song_id = null;
			let metronome_bpm = 60;
			let metronome_bpm_display = 60;
			let metronome_time_signature = '4/4';

			$('#songs').on('click', '> li', function() {
				current_song_id          = $(this).data('song-id');
				metronome_bpm            = $(this).data('bpm');
				metronome_time_signature = $(this).data('time_signature');

				current_song_pos = $(this).index();
				updatePrevNextBtns();
				$(this).parent().find('li').removeClass('current-song');
				$(this).addClass('current-song');

				metronome_bpm_display = metronome_bpm;
				$('#bpm-value').text(metronome_bpm_display);

				processEighthNotes();
				enableMetronomeButtons();

				if (metronome_interval) {
					stopMetronome();
					startMetronome();
				}
			});

			$('#edit-song-list-btn').click( function() {
				window.location = '/song-lists/edit/'+ songList_id;
			});

			$('.metronome-change-btn').click( function() {
				if ($(this).hasClass('minus')) {
					if (metronome_bpm_display > 20) {
						metronome_bpm_display -= 1;
					}
				} else {
					metronome_bpm_display += 1;
				}

				metronome_bpm = metronome_bpm_display;
				processEighthNotes();

				$('#bpm-value').text(metronome_bpm_display);
				if (metronome_interval) {
					stopMetronome();
					startMetronome();
				}
			});

			// Add eighth notes when necessary
			function processEighthNotes() {
				metronome_bpm = metronome_bpm_display
				if (metronome_time_signature != '6/8' && metronome_bpm_display < 90) {
					metronome_bpm *= 2;
				} else if (metronome_time_signature == '6/8') {
					metronome_bpm *= 3;
				}
			}

			function updatePrevNextBtns() {
				// First song of the list
				if (current_song_pos == 0) {
					$('#prev-btn').addClass('inactive');
				} else if (current_song_pos !== null) {
					$('#prev-btn').removeClass('inactive');
				}

				// Last song of the list
				if (current_song_pos == song_list_length - 1) {
					$('#next-btn').addClass('inactive');
				} else if (current_song_pos !== null) {
					$('#next-btn').removeClass('inactive');
				}
			}

			function startMetronome() {
				processEighthNotes();
				let interval = 1000 / (metronome_bpm / 60);

				metronomeBeat();
				metronome_interval = setInterval(function() {
					metronomeBeat();
				}, interval);

				$('#play-btn')
					.removeClass('fa-play-circle')
					.addClass('fa-stop-circle');
			};

			var audio = new Audio('/sounds/tick.mp3');
			function metronomeBeat() {
				audio.play();

				// Visual aid
				let current_steps = $('.metronome-steps .active').length;
				if ((metronome_time_signature == '4/4' && current_steps == 4) || (metronome_time_signature != '4/4' && current_steps == 3)) {
					$('.metronome-steps .active').removeClass('active');
					$('.metronome-steps > span').eq(0).addClass('active');
				} else {
					$('.metronome-steps > span').eq(current_steps).addClass('active');
				}
			}

			function stopMetronome() {
				clearInterval(metronome_interval);
				metronome_interval = null;
				$('#play-btn')
					.removeClass('fa-stop-circle')
					.addClass('fa-play-circle');
				$('#bpm-value').removeClass('beat');
				$('.metronome-steps > span').removeClass('active');
			}

			function enableMetronomeButtons() {
				$('.metronome-btns').removeClass('inactive');
			};

			function nextSong() {
				if ($('#next-btn').hasClass('inactive')) {
					return false;
				}

				let $next_song = $('#songs > li[data-song-id="'+ current_song_id +'"]').next();
				if ($next_song.length) {
					$next_song.click();
					if (metronome_interval) {
						stopMetronome();
						startMetronome();
					}
				}
			}

			function prevSong() {
				if ($('#prev-btn').hasClass('inactive')) {
					return false;
				}

				let $prev_song = $('#songs > li[data-song-id="'+ current_song_id +'"]').prev();
				if ($prev_song.length) {
					$prev_song.click();
					if (metronome_interval) {
						stopMetronome();
						startMetronome();
					}
				}
			}

			$('#play-btn').click( function() {
				if ($(this).hasClass('fa-play-circle')) {
					startMetronome()
				} else {
					stopMetronome()
				}
			});

			$('#next-btn').click(nextSong);
			$('#prev-btn').click(prevSong);
			$(document).keydown( function(e) {
				switch(e.which) {
					case 32: // spacebar
						if (!$('.metronome-btns').hasClass('inactive')) {
							if (metronome_interval) {
								stopMetronome();
							} else {
								startMetronome();
							}
						}
					break;

					case 37: // left
						prevSong();
					break;

					case 38: // up: TODO: increase tempo
						$('.metronome-change-btn.plus').click();
					break;

					case 39: // right
						nextSong();
					break;

					case 40: // down: TODO: decrease tempo
						$('.metronome-change-btn.minus').click();
					break;

					default: return; // exit this handler for other keys
				}
				e.preventDefault();
			});
		});
	</script>
@endsection